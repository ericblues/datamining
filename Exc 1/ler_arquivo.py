#!/usr/bin/python
# -*- coding: utf-8 -*-

matriz = []
matrizCandidata = []
matrizAprovada = []

def leia(texto):
    arquivo = open(texto);
    for linha in arquivo.readlines():
        dados = linha.split()
        matriz.append(dados)


def preencheCandidata():
    print "teste"

def fConf1(at, pr):
    #Variaveis para gerar o fator superior
    xy = 0 #representa a união de x E y
    x = 0 #representa a qtde de vezes q apenas x é sim
    conMin = 0.8

    #para controle das comparações
    atual = at
    proximo = pr

    print "Os seguintes produtos passaram no Fconf1"

    while atual < len(matriz[0]):
        while proximo < len(matriz[0]):
            if (atual != proximo):
                for elemento in matriz:
                    if (elemento[atual] == "1"):
                        if (elemento[proximo] == "0"):
                            x += 1
                        if (elemento[proximo] == "1"):
                            xy += 1
                        #print elemento [atual] + " | " + elemento [proximo]

            #verifiação se o xy e x são diferentes de 0
            if (xy != 0 and x != 0):
                #verifica se o valor é maior ou igual supMin
                if (float(xy) / x >= conMin):
                    print matriz[0][atual] + " | " + matriz[0][proximo]
            xy = 0
            x = 0
            proximo += 1
        atual += 1
        proximo = 0



def fConf2(at, pr, xy):
    #Variaveis para gerar o fator superior
    #xy representa a união de x E y
    x = 0 #representa a qtde de vezes q apenas x é sim
    conMin = 0.8

    #para controle das comparações
    atual = at
    proximo = pr


    for vetor in matriz:
        if (vetor[atual] == "1"):
            x += 1
    #verifica se o valor é maior ou igual supMin
    resultado = float(xy) / x
    if (resultado >= conMin):
        #print matriz[0][atual] + " | " + matriz[0][proximo] + " - FCONF OK"
        matrizAprovada.append(matriz[0][atual] + " | " + matriz[0][proximo] + " | Fconf = " + str(resultado))




def fSup(min):


    #Variaveis para gerar o fator superior
    xy = 0 #representa a união de x E y
    n = len(matriz) - 1  #representa o numero de tuplas
    teste = 0
    supMin = min

    #para controle das comparações
    atual = 0
    proximo = atual + 1


    #matriz no indice 0 pega a primeira lista na matriz
    #como o tamanho de todas as listas na matriz são iguais
    #pegando apenas a primeira lista para saber o tamanho ja é o suficiente
    while atual < len(matriz[0]):
        while proximo < len(matriz[0]):
            for elemento in matriz:
                #verifica se x e y são iguais
                if (elemento[atual] == "1" and elemento[proximo] == "1"):
                    #em caso positivo, xy é icrementado para realizar o calculo de validação
                    xy += 1
                    #print elemento [atual] + " | " + elemento [proximo]

            #verifiação se o xy é diferente de 0
            if (xy != 0):
                #verifica se o valor é maior ou igual supMin
                resultado = float(xy) / n
                if (resultado >= supMin):
                    #print matriz[0][atual] + " | " + matriz[0][proximo] + " - FSUP OK"
                    matrizCandidata.append(matriz[0][atual] + " | " + matriz[0][proximo] + " | Fsup = " + str(resultado))
                    #verifica a ida
                    fConf2(atual, proximo, xy)
                    #verifica a volta
                    fConf2(proximo, atual, xy)
            xy = 0
            proximo += 1
        atual += 1
        proximo = atual + 1



def menuPrincipal():
    opcao = " "
    ler_arquivo = ""
    supMin = 0
    print "Escolha um arquivo para fazer a mineracao: "
    print "1 - Super Mercado"
    print "2 - Locadora"

    opcao = raw_input()

    if (opcao == "1"):
        ler_arquivo = "arquivo.txt"
        supMin = 0.3
    if (opcao == "2"):
        ler_arquivo = "locadora.txt"
        supMin = 0.3

    leia(ler_arquivo)
    fSup(supMin)
    print "\n"
    print "Passaram no FSUP"
    for linha in matrizCandidata:
        print linha
    print "Total: " + str(len(matrizCandidata))
    print "\n"
    print "Passaram no FCONF"
    for linha in matrizAprovada:
        print linha
    print "Total: " + str(len(matrizAprovada))
    #fConf1(conMin)


if __name__ == '__main__':
    menuPrincipal()
