#!/usr/bin/python
# -*- coding: utf-8 -*-


matriz = []
medias = []

def similaridade():

    i = 0
    somatorioA = 0
    somatorioB = 0
    somatorioC = 0
    divisor = 0
    distancia = 0

    for atual in range(1, len(matriz[0])):
        input()

        for proximo in range(atual + 1, len(matriz[0])):

            somatorioA = 0
            somatorioB = 0
            somatorioC = 0
            divisor = 0
            if (atual != proximo):
                input()
                for linha in range(1, len(matriz)):
                    if (matriz[linha][atual] != "-" and matriz[linha][proximo] != "-"):
                        print("("+matriz[linha][atual] + " - " + str(medias[linha-1]) + ")("+matriz[linha][proximo]+" - "+str(medias[linha-1])+")")

                        somatorioA += (float(matriz[linha][atual]) - float(medias[linha - 1]))*(float(matriz[linha][proximo]) - float(medias[linha - 1]))
                        somatorioB += ((float(matriz[linha][atual])-float(medias[linha - 1]))**2)
                        somatorioC += ((float(matriz[linha][proximo])-float(medias[linha - 1]))**2)

                somatorioB = somatorioB ** (0.5)
                somatorioC = somatorioC ** (0.5)

                divisor = somatorioB * somatorioC

                print("Divisao: " + str(somatorioA) + " / " + str(divisor))
                distancia = somatorioA / divisor

                print("Distancia entre " + matriz[0][atual] + " e " + matriz[0][proximo] + " = " + str(distancia))


    print()

def media():
    m = 0
    i = 0

    for linha in range(1, len(matriz[0])):
        #print (matriz[linha][0])
        m = 0
        i = 0
        for coluna in range(1, len(matriz[0])):
            #print (matriz[linha][coluna])
            if (matriz[linha][coluna] != "-"):
                i += 1
                m += float(matriz[linha][coluna])
        m = m / i
        medias.append(m)
        print(matriz[linha][0] + " media => " + str(m) + "\n\n")

    similaridade()


def leia():
    arquivo = open("arquivo.txt")
    for linha in arquivo.readlines():
        dados = linha.split()
        matriz.append(dados)
    media()



def menuPrincipal():
    leia()

if __name__ == '__main__':
    menuPrincipal()
